Adventure Land

Library requirements:
* glfw
* glm
* soil

Features of Part 1
* Can move the cannon using regular arrow keys.
* Can move the barrel of the cannon using L/R keys.
* Can jump using spacebar.
* If the cannon moves or jumps over a missing land piece the cannon will have to restart 
  its journey from the beginning. The cannon will be taken to starting position if it jumps/moves 
  over an empty tile.
* If the cannon moves/jumps over a black box it will have to start its journey again.
* The cannon can pick the gold block.
* This part has 3 camera views
  1.) Follow cam view - This is the default view.Enable this view by pressing F1.
  2.) Tower view - Enable by pressing F2.
  3.) Top view - Enable by pressing F3.